function dbinitcql() {
    if [ -n "$1" ]
    then
        echo $1
        kubectl get pods | grep dz-cassandra | xargs | cut -f 1 -d ' ' | xargs -I {} kubectl exec {} -- bash -c "cd /tmp;cqlsh -f $1"
	echo " Deleting dbinit.cql "
	kubectl get pods | grep dz-cassandra | xargs | cut -f 1 -d ' ' | xargs -I {} kubectl exec {} -- bash -c "cd /tmp; rm $1"
	echo $1 ": Deleted"
    else
        echo $1
        echo "No CQL found"
    fi
}

function dbinitpsql() {
    if [ -n "$1" ]
    then
        echo $1
        kubectl get pods | grep dz-postgres | xargs | cut -f 1 -d ' ' | xargs -I {} kubectl exec {} -- bash -c "cd /tmp;psql -U postgres -h localhost -f $1"
	echo " Deleting dbinit.psql"
        kubectl get pods | grep dz-postgres | xargs | cut -f 1 -d ' ' | xargs -I {} kubectl exec {} -- bash -c "cd /tmp; rm $1"
	echo $1 ": Deleted"
    else
        echo $1
        echo "No PSQL found"
    fi
}

function setroutes() {
    if [ -n "$3" ]
    then
        cd $1
        $3 $2
        cd ..
    else
        echo $1 $2
        echo "No Routes for $1"
    fi
}

