#!/bin/bash
echo " Set route------ :   "

# Run  Route all
#copy routeall to kong

kong_pod=`kubectl get pods | grep dz-kong | xargs | cut -f 1 -d ' '`
echo "Copying post route scripts to kong : "  $kong_pod
kubectl cp routeall.sh  $kong_pod:/tmp/

# install curl and curl-dev on kong and proceed with routing
kubectl exec -it $kong_pod  -- bash -c "apk update; apk add curl; apk add curl-dev"
kubectl exec -it $kong_pod -- bash -c "cd /tmp;bash routeall.sh"


cd post
./postscript.sh

cd ..
echo " All done "

kubectl get pods -o wide --show-all
kubectl get svc --show-all
