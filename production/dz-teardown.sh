#!/bin/bash

# Removing pods, Deployments services 

echo "Deleting DB's"
kubectl delete -f services/dz-databases/
echo " Let's delete Kong and kafka "
kubectl delete -f services/dz-kong/
kubectl delete -f services/dz-kafka/

echo "Deleting Beacon engine and services "
kubectl delete -f services/dz-beacon-service/
kubectl delete -f services/dz-beacon-engine/

echo "Deleting Beacon engine admin frontend Backend and ui"
kubectl delete -f services/dz-ui/
kubectl delete -f services/dz-admin-ui/
kubectl delete -f services/dz-admin-backend/
kubectl delete -f services/dz-admin-frontend/

echo "Deleting broker analytics wrapper zookeeper and management"
kubectl delete -f services/dz-message-broker/
kubectl delete -f services/dz-analytics-proxy/
kubectl delete -f services/dz-cache-wrapper/
kubectl delete -f services/dz-configuration-management/
kubectl delete -f services/dz-user-management-service/
kubectl delete -f services/dz-zookeeper/

echo "Deleting Connectors "
kubectl delete -f services/dz-connector-admin/
kubectl delete -f services/dz-connectors/



# Removing Datadog pods

#kubectl delete -f dz-datadog-agent.yaml 

#removing pv and pvc

kubectl delete pv --all
kubectl delete pvc --all

# Removing Files 

rm -rf servicefiles

echo " All done "
