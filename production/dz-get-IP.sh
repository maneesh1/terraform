#!/bin/bash

#Get the IP of pods to be exposed

kongIP=`kubectl get pods --show-all -o wide | grep dz-kong | awk {'print $6'}`
uiIP=`kubectl get pods --show-all -o wide | grep dz-ui | awk {'print $6'}`
beaconserviceIP=`kubectl get pods --show-all -o wide | grep dz-beacon-service | awk {'print $6'}`
adminuiIP=`kubectl get pods --show-all -o wide | grep dz-admin-ui | awk {'print $6'}`
messagebrokerIP=`kubectl get pods --show-all -o wide | grep dz-message-broker | awk {'print $6'}`
adminbackendIP=`kubectl get pods --show-all -o wide | grep dz-admin-backend | awk {'print $6'}`
adminfrontendIP=`kubectl get pods --show-all -o wide | grep dz-admin-frontend | awk {'print $6'}`
connectoradminIP=`kubectl get pods --show-all -o wide | grep dz-connector-admin | awk {'print $6'}`
analyticsproxyIP=`kubectl get pods --show-all -o wide | grep dz-analytics-proxy | awk {'print $6'}`
beaconengineIP=`kubectl get pods --show-all -o wide | grep dz-beacon-engine | awk {'print $6'}`




#configurationmanagementIP=`kubectl get pods --show-all -o wide | grep dz-configuration-management | awk {'print $6'}`
#usermanagementserviceIP=`kubectl get pods --show-all -o wide | grep dz-user-management-service | awk {'print $6'}`


#echo "kong " $kongIP
