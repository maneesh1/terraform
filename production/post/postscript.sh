#!/bin/bash

#Get the postgres, kong and cassandra pod
postgres_pod=`kubectl get pods | grep dz-postgres | xargs | cut -f 1 -d ' '`
kong_pod=`kubectl get pods | grep dz-kong | xargs | cut -f 1 -d ' '`
beaconengine_pod=`kubectl get pods | grep dz-beacon-engine | xargs | cut -f 1 -d ' '`

#Run post setup DB scripst on postgres
kubectl cp rolemasters.psql $postgres_pod:/tmp/
kubectl exec -it $postgres_pod -- bash -c "cd /tmp;psql -U postgres -h localhost -f rolemasters.psql"


#copy post-routes to kong
echo "Copying post route scripts to kong"
kubectl cp kongScripts.sh  $kong_pod:/tmp/

# install curl and curl dev on kong and proceed with routing
kubectl exec -it $kong_pod  -- bash -c "apk update; apk add curl; apk add curl-dev"
kubectl exec -it $kong_pod -- bash -c "cd /tmp;bash kongScripts.sh"

#Run sdk init on cassandra
kubectl cp sdk.cql dz-cassandra-0:/tmp/
kubectl exec -it dz-cassandra-0 -- bash -c "cd /tmp;cqlsh -f sdk.cql"


#Run metadata on beacon engine 
kubectl cp beaconengineScripts.sh  $beaconengine_pod:/tmp/
echo "Running curl  on Beacon engine : " $beaconengine_pod
kubectl exec -it $beaconengine_pod -- bash -c "apk update; apk add curl; apk add curl-dev"
kubectl exec -it $beaconengine_pod -- bash -c "cd /tmp; bash beaconengineScripts.sh"
