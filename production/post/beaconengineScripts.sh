#!/bin/bash

echo "+++++++++++++++++++++++++Inside Beacon Engine+++++++++++++++++++++++++"

echo "Adding PLayesrs "

curl -X POST \
  http://localhost:3939/v1/players/batch \
  -H 'Content-Type: application/json' \
  -d '[
  {
    "playerName": "html",
    "playerDescription": "HTML 5 Player",
    "playerVersion": "5",
    "playerEvents": [
      {
        "eventIcon": "",
        "playerEvent": "PLAY"
      },
      {
        "eventIcon": "",
        "playerEvent": "PAUSE"
      },
      {
        "eventIcon": "",
        "playerEvent": "PLAYHEAD"
      },
      {
        "eventIcon": "",
        "playerEvent": "SEEK"
      }
    ],
    "metadata": [
      {
        "metadata": "Title",
        "required": true
      },
      {
        "metadata": "Device",
        "required": true
      },
      {
        "metadata": "CDN",
        "required": true
      }
    ],
    "playerFluxData": [
      {
        "fluxData": "Throughput",
        "required": true
      },
      {
        "fluxData": "Bitrate",
        "required": true
      },
      {
        "fluxData": "Play Head Position",
        "required": true
      }
    ],
    "createdBy": "hardikkumar@datazoom.io"
  },
  {
    "playerName": "theoplayer",
    "playerDescription": "Theo player",
    "playerVersion": "2",
    "playerEvents": [
      {
        "eventIcon": "",
        "playerEvent": "PLAY"
      },
      {
        "eventIcon": "",
        "playerEvent": "PAUSE"
      },
      {
        "eventIcon": "",
        "playerEvent": "PLAYHEAD"
      },
      {
        "eventIcon": "",
        "playerEvent": "SEEK"
      }
    ],
    "metadata": [
      {
        "metadata": "Title",
        "required": true
      },
      {
        "metadata": "Device",
        "required": true
      },
      {
        "metadata": "CDN",
        "required": true
      }
    ],
    "playerFluxData": [
      {
        "fluxData": "Throughput",
        "required": true
      },
      {
        "fluxData": "Bitrate",
        "required": true
      },
      {
        "fluxData": "Play Head Position",
        "required": true
      }
    ],
    "createdBy": "hardikkumar@datazoom.io"
  },
  {
    "playerName": "jwplayer",
    "playerDescription": "JW player",
    "playerVersion": "8.0",
    "playerEvents": [
      {
        "eventIcon": "",
        "playerEvent": "PLAY"
      },
      {
        "eventIcon": "",
        "playerEvent": "PAUSE"
      },
      {
        "eventIcon": "",
        "playerEvent": "PLAYHEAD"
      },
      {
        "eventIcon": "",
        "playerEvent": "SEEK"
      }
    ],
    "metadata": [
      {
        "metadata": "Title",
        "required": true
      },
      {
        "metadata": "Device",
        "required": true
      },
      {
        "metadata": "CDN",
        "required": true
      }
    ],
    "playerFluxData": [
      {
        "fluxData": "Throughput",
        "required": true
      },
      {
        "fluxData": "Bitrate",
        "required": true
      },
      {
        "fluxData": "Play Head Position",
        "required": true
      }
    ],
    "createdBy": "hardikkumar@datazoom.io"
  },
  {
    "playerName": "anvatoplayer",
    "playerDescription": "Anvato Player",
    "playerVersion": "3",
    "playerEvents": [
      {
        "eventIcon": "",
        "playerEvent": "PLAY"
      },
      {
        "eventIcon": "",
        "playerEvent": "PAUSE"
      },
      {
        "eventIcon": "",
        "playerEvent": "PLAYHEAD"
      },
      {
        "eventIcon": "",
        "playerEvent": "SEEK"
      }
    ],
    "metadata": [
      {
        "metadata": "Title",
        "required": true
      },
      {
        "metadata": "Device",
        "required": true
      },
      {
        "metadata": "CDN",
        "required": true
      }
    ],
    "playerFluxData": [
      {
        "fluxData": "Throughput",
        "required": true
      },
      {
        "fluxData": "Bitrate",
        "required": true
      },
      {
        "fluxData": "Play Head Position",
        "required": true
      }
    ],
    "createdBy": "hardikkumar@datazoom.io"
  },
  {
    "playerName": "brightcoveplayer",
    "playerDescription": "Brightcove Player",
    "playerVersion": "6",
    "playerEvents": [
      {
        "eventIcon": "",
        "playerEvent": "PLAY"
      },
      {
        "eventIcon": "",
        "playerEvent": "PAUSE"
      },
      {
        "eventIcon": "",
        "playerEvent": "PLAYHEAD"
      },
      {
        "eventIcon": "",
        "playerEvent": "SEEK"
      }
    ],
    "metadata": [
      {
        "metadata": "Title",
        "required": true
      },
      {
        "metadata": "Device",
        "required": true
      },
      {
        "metadata": "CDN",
        "required": true
      }
    ],
    "playerFluxData": [
      {
        "fluxData": "Throughput",
        "required": true
      },
      {
        "fluxData": "Bitrate",
        "required": true
      },
      {
        "fluxData": "Play Head Position",
        "required": true
      }
    ],
    "createdBy": "hardikkumar@datazoom.io"
  }
]'
 
echo "Added Player patches"
echo "++++++++++++++++++++++++++++++++++++++++++++++"

echo "Adding jwplayer "
 
curl -X POST \
  http://localhost:3939/v1/customer_events \
  -H 'Content-Type: application/json' \
  -d '{
  "configurationId": "dd6387ccb-e7f8-43cb-bca4-ab40fd612fff",
  "createdBy": "hardikkumar@datazoom.io",
  "customerCode": "a6387ccb-e7f8-43cb-bca4-ab40fd612fee",
  "eventType": "CREATE",
  "connectorList":
    ["7dc4b876-0a61-4c4f-827f-ded09d8b8c04",
  "100bec80-b74f-4c86-a393-391befecd393"
    ],
  "dzApp": true,
  "players": [
    {
      "eventsToBeCaptured": [
        "PLAY","PAUSE","SEEK","PLAYHEAD"
      ],
      "name": "jwplayer",
      "version": "8.0"
    }
  ]
}'

echo "Added jwplayer "

echo "------------------------------------------------"

echo "Adding anvatoplayer"
 
curl -X POST \
  http://localhost:3939/v1/customer_events \
  -H 'Content-Type: application/json' \
  -d '{
  "configurationId": "ebb697fc-945b-4142-94e0-dbbad0c1d6f2",
  "createdBy": "hardikkumar@datazoom.io",
  "customerCode": "a6387ccb-e7f8-43cb-bca4-ab40fd612fee",
  "eventType": "CREATE",
  "connectorList":
    ["7dc4b876-0a61-4c4f-827f-ded09d8b8c04",
  "100bec80-b74f-4c86-a393-391befecd393"
    ],
  "dzApp": true,
  "players": [
    {
      "eventsToBeCaptured": [
       "PLAY","PAUSE","SEEK","PLAYHEAD"
      ],
      "name": "anvatoplayer",
      "version": "3"
    }
  ]
}'

echo "Added  anvatoplayer"
echo "-----------------------------------------------"

echo "Adding theoplayer" 
curl -X POST \
  http://localhost:3939/v1/customer_events \
  -H 'Content-Type: application/json' \
  -d '{
  "configurationId": "53a8eb66-eab5-4803-a1fd-a590a4a762a2",
  "createdBy": "hardikkumar@datazoom.io",
  "customerCode": "a6387ccb-e7f8-43cb-bca4-ab40fd612fee",
  "eventType": "CREATE",
  "connectorList":
    ["7dc4b876-0a61-4c4f-827f-ded09d8b8c04",
  "100bec80-b74f-4c86-a393-391befecd393"
    ],
  "dzApp": true,
  "players": [
    {
      "eventsToBeCaptured": [
       "PLAY","PAUSE","SEEK","PLAYHEAD"
      ],
      "name": "theoplayer",
      "version": "2"
    }
  ]
}'
 
echo "Added theoplayer"

echo "------------------------------------------"
echo "Adding html player"
 
curl -X POST \
  http://localhost:3939/v1/customer_events \
  -H 'Content-Type: application/json' \
  -d '{
  "configurationId": "ee6387ccb-e7f8-43cb-bca4-ab40fd612fff",
  "createdBy": "hardikkumar@datazoom.io",
  "customerCode": "a6387ccb-e7f8-43cb-bca4-ab40fd612fee",
  "eventType": "CREATE",
  "connectorList":
    ["7dc4b876-0a61-4c4f-827f-ded09d8b8c04",
  "100bec80-b74f-4c86-a393-391befecd393"
    ],
  "dzApp": true,
  "players": [
    {
      "eventsToBeCaptured": [
        "PLAY","PAUSE","SEEK","PLAYHEAD"
      ],
      "name": "html",
      "version": "5"
    }
  ]
}'

echo "Added html playeer "
 
echo "---------------------------------------------"
echo "Adding brightcoveplayer" 
curl -X POST \
  http://localhost:3939/v1/customer_events \
  -H 'Content-Type: application/json' \
  -d '{
  "configurationId": "dff415b1-eab4-4f39-b450-c026ade5e4b6",
  "createdBy": "hardikkumar@datazoom.io",
  "customerCode": "a6387ccb-e7f8-43cb-bca4-ab40fd612fee",
  "eventType": "CREATE",
  "connectorList":
    ["7dc4b876-0a61-4c4f-827f-ded09d8b8c04",
  "100bec80-b74f-4c86-a393-391befecd393"
    ],
  "dzApp": true,
  "players": [
    {
      "eventsToBeCaptured": [
        "PLAY","PAUSE","SEEK","PLAYHEAD"
      ],
      "name": "brightcoveplayer",
      "version": "6"
    }
  ]
}'


echo "Added brightcoveplayer"
echo "----------------------------------------------"
