#!/bin/bash



echo "+++++++++++++++++++++++++++Inside KONG++++++++++++++++++++++++++++++++++++++++++"


curl -X POST "http://localhost:8000/admin-backend/companies/saveCompanyData" -H "accept: */*" -H "Content-Type: application/json" -d "{\"company_name\":\"Datazoom\", \"city\" : \"New York\", \"state\" : \"New York\", \"postcode\": \"10016\", \"is_blocked\": \"n\"}"

echo "Adding admin user to app"

curl -X POST "http://localhost:8000/admin-backend/users/saveUserData" -H "accept: */*" -H "Content-Type: application/json" -d "{\"role_id\":3,\"company_id\":1,\"email\":\"manaf@datazoom.io\",\"full_name\":\"Manaf Karim\",\"created_by\":1,\"is_blocked\" :\"y\"}"

echo "Added admin user manaf@datazoom.io "
echo "--------------------------------------------------------"

echo "Adding connector admin pathes"

echo "--------------------------------------------------------"


echo "Adding Amplitude"

curl -X POST \
  http://localhost:8000/connectoradmin/v1/connector_details \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
  "apiUrl": "https://api.amplitude.com/httpapi",
  "connectorName": "Amplitude",
  "createdBy": "hardikkumar@datazoom.io",
  "icon": "amplitude",
  "keyName": "api-key"
}'
echo "Added Amplitude"
echo "--------------------------------------------------------"

echo "AddingGoogle Analytics"


curl -X POST \
  http://localhost:8000/connectoradmin/v1/connector_details \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
  "apiUrl": "",
  "connectorName": "Google Analytics",
  "createdBy": "hardikkumar@datazoom.io",
  "icon": "",
  "keyName": "Tracking ID"
}'

echo "Added Google Analytics"
echo "--------------------------------------------------------"

echo "Adding  Heap Analytics"

curl -X POST \
  http://localhost:8000/connectoradmin/v1/connector_details \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
  "apiUrl": "",
  "connectorName": "Heap Analytics",
  "createdBy": "hardikkumar@datazoom.io",
  "icon": "",
  "keyName": "App ID,User Identity"
}'


echo "Added Heap Analytics"
echo "--------------------------------------------------------"

echo "Adding YOUBORA Analytics"

curl -X POST \
  http://localhost:8000/connectoradmin/v1/connector_details \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
  "apiUrl": "",
  "connectorName": "YOUBORA Analytics",
  "createdBy": "hardikkumar@datazoom.io",
  "icon": "",
  "keyName": "Account Code,Data URL,Plugin Version"
}'

echo "Added YOUBORA Analytics"
echo "--------------------------------------------------------"

echo "Adding  New Relic"

curl -X POST \
  http://localhost:8000/connectoradmin/v1/connector_details \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
  "apiUrl": "",
  "connectorName": "New Relic",
  "createdBy": "hardikkumar@datazoom.io",
  "icon": "",
  "keyName": "Insert Key"
}'
echo "Added New Relic"
echo "--------------------------------------------------------"

echo "Adding AWS Kinesis"

curl -X POST \
  http://localhost:8000/connectoradmin/v1/connector_details \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
  "apiUrl": "",
  "connectorName": "AWS Kinesis",
  "createdBy": "hardikkumar@datazoom.io",
  "icon": "",
  "keyName": "Stream name"
}'

echo "Added AWS Kinesis"
echo "--------------------------------------------------------"

echo "Adding  Adobe Analytics"

curl -X POST \
  http://localhost:8000/connectoradmin/v1/connector_details \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d '{
  "apiUrl": "",
  "connectorName": "Adobe Analytics",
  "createdBy": "hardikkumar@datazoom.io",
  "icon": "",
  "keyName": "Report Suite Id"
}'

echo "Added Adobe Analytics"
echo "--------------------------------------------------------"
