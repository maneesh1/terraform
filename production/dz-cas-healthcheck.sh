#!/bin/bash

#Get the cassandra pod 

cassandra_pod=`kubectl get pods | grep dz-cassandra | awk '{print $1}' | grep -v NAME | xargs`


servicelist=($cassandra_pod)

status='1/1'
retries_health=0
for (( ; ; ))
do
    error_health=0
    for i in "${!servicelist[@]}"
    do
      echo "Service: ${servicelist[$i]} : $i"
      kubectl get pods  | grep ${servicelist[$i]}  | awk {' print $2'}
      health_status=`kubectl get  pods  | grep ${servicelist[$i]} | awk {' print $2'}`
        if [ "$health_status" != "$status" ]
        then
          echo "Service: ${servicelist[$i]} not healthy"
          error_health=1
          break
        fi
    done
    
    retries_health=$((retries_health+1))

    if [ "$error_health" -ne "0" ]; then
        sleep 10
        if [ "$retries_health" -gt 20 ]
        then
            break
        fi
    else
        break
    fi
    
done

