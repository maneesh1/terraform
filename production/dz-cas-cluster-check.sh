#!/bin/bash
#Get the cassandra cluster pods

cassandra_pod=`kubectl get pods | grep dz-cassandra | awk '{print $1}' | grep -v NAME | xargs`



servicelist=($cassandra_pod)

status='UN UN UN'
retries_health=0
for (( ; ; ))
do
    error_health=0
    for i in "${!servicelist[@]}"
    do
      echo "Service: ${servicelist[$i]} : $i"
      health_status=`kubectl exec -it dz-cassandra-0 nodetool status | grep rack | awk {' print $1'} | xargs `
        if [ "$health_status" != "$status" ]
        then
          echo "Cluster: Cassandra not ready"
          error_health=1
          break
        fi
    done
    
    retries_health=$((retries_health+1))

    if [ "$error_health" -ne "0" ]; then
        sleep 10
        if [ "$retries_health" -gt 20 ]
        then
            break
        fi
    else
        break
    fi
    
done

