#!/bin/bash

services[0]="ui"
servicesdir[0]="ui"
servicesroutes[0]="./routeall.sh"

services[1]="user-events-library"
servicesdir[1]="user-events-library"

services[2]="beacon-engine"
servicesdir[2]="beacon-engine"
servicescqlinit[2]="./dbinit.cql"

services[3]="beacon-service"
servicesdir[3]="beacon-service"

services[4]="user-management-service"
servicesdir[4]="user-management-service"
servicescqlinit[4]="./dbinit.cql"

services[5]="configuration-management"
servicesdir[5]="configuration-management"
servicescqlinit[5]="./dbinit.cql"

services[6]="parse-ly"
servicesdir[6]="parse-ly"

services[7]="connector-keenio"
servicesdir[7]="connector-keenio"

services[8]="analytics-proxy"
servicesdir[8]="analytics-proxy"
servicescqlinit[8]="./dbinit.cql"

services[9]="admin-ui"
servicesdir[9]="admin-ui"

services[10]="message-broker"
servicesdir[10]="message-broker"
servicescqlinit[10]="./dbinit.cql"

