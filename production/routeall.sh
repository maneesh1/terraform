#!/bin/bash

echo "Route Start"
# Place Routes Here !!!

curl -X DELETE http://localhost:8001/apis/admin-backend
curl -4 -i -X POST   --url http://localhost:8001/apis/   --data 'name=admin-backend'   --data 'uris=/admin-backend' --data "upstream_url=http://dz-admin-backend:8051"
curl -4 -X GET http://localhost:8000/admin-backend

curl -X DELETE http://localhost:8001/apis/dz-admin-frontend-api
curl -4 -i -X POST   --url http://localhost:8001/apis/   --data 'name=dz-admin-frontend-api'   --data 'uris=/dzadminfrontendapi' --data "upstream_url=http://dz-admin-frontend:80"
curl -4 -X GET http://localhost:8000/dzadminfrontendapi

curl -X DELETE http://localhost:8001/apis/dz-beacon-service
curl -4 -i -X POST   --url http://localhost:8001/apis/   --data 'name=dz-beacon-service'   --data 'uris=/beacon' --data "upstream_url=http://dz-beacon-service:2000"
curl -4 -X GET http://localhost:8000/beacon

curl -X DELETE http://localhost:8001/apis/dz-connector-admin
curl -4 -i -X POST   --url http://localhost:8001/apis/   --data 'name=dz-connector-admin'   --data 'uris=/connectoradmin' --data "upstream_url=http://dz-connector-admin:8183"
curl -4 -X GET http://localhost:8000/connectoradmin

curl -X DELETE http://localhost:8001/apis/dz-message-broker
curl -4 -i -X POST   --url http://localhost:8001/apis/   --data 'name=dz-message-broker'   --data 'uris=/logger' --data "upstream_url=http://dz-message-broker:3000"
curl -4 -X GET http://localhost:8000/logger

curl -X DELETE http://localhost:8001/apis/dz-ui-api
curl -4 -i -X POST   --url http://localhost:8001/apis/   --data 'name=dz-ui-api'   --data 'uris=/dzuiapi' --data "upstream_url=http://dz-ui:19000"
curl -4 -X GET http://localhost:8000/dzuiapi

curl -X DELETE http://localhost:8001/apis/dz-admin-ui
curl -4 -i -X POST   --url http://localhost:8001/apis/   --data 'name=dz-admin-ui'   --data 'uris=/adminui' --data "upstream_url=http://dz-admin-ui:80"
curl -4 -X GET http://localhost:8000/adminui


curl -X DELETE http://localhost:8001/apis/beacon-engine
curl -4 -i -X POST   --url http://localhost:8001/apis/   --data 'name=beacon-engine'   --data 'uris=/beacon-engine' --data "upstream_url=http://dz-beacon-engine:3939"
curl -4 -X GET http://localhost:8000/beacon-engine

curl -X DELETE http://localhost:8001/apis/analytics-proxy
curl -4 -i -X POST   --url http://localhost:8001/apis/   --data 'name=analytics-proxy'   --data 'uris=/analytics-proxy' --data "upstream_url=http://dz-analytics-proxy:3001"
curl -4 -X GET http://localhost:8000/analytics-proxy


echo "Route End"

