#!/bin/bash

#Get the postgres pod
postgres_pod=`kubectl get pods | grep dz-postgres | xargs | cut -f 1 -d ' '`

#Run datadog init setup DB scripst on postgres
kubectl cp datadog.psql $postgres_pod:/tmp/
kubectl exec -it $postgres_pod -- bash -c "cd /tmp;psql -U postgres -h localhost -f datadog.psql"

