Installation


kubectl apply -f kube-state-metrics

kubectl create -f dd-agent-rbac.yaml

kubectl create -f datadog-agent.yaml


To capture PostgreSQL metrics :

Make sure to create a read-only datadog user with proper access to your PostgreSQL Server. I have
used same in datadog-agent.yaml. Please run following command to create a user with password.


create user datadog with password '5qeI938pOPAg84m77ZmA9iHm';
grant SELECT ON pg_stat_database to datadog;




