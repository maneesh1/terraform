apiVersion: extensions/v1beta1
kind: DaemonSet
metadata:
  name: datadog-agent
spec:
  template:
    metadata:
      labels:
        app: datadog-agent
      name: datadog-agent
    spec:
      serviceAccountName: datadog
      tolerations:
      - key: node-role.kubernetes.io/master
        effect: NoSchedule
      containers:
      - image: datadog/agent:latest
        imagePullPolicy: Always
        name: datadog-agent
        ports:
          - containerPort: 8125
            name: dogstatsdport
            protocol: UDP
          - containerPort: 8126
            name: traceport
            protocol: TCP
        env:
          - name: DD_API_KEY
            value: 3272540553acc86ad6931d0f00b18828
          - name: DD_PROCESS_AGENT_ENABLED
            value: "true"
          - name: DD_COLLECT_KUBERNETES_EVENTS
            value: "true"
          - name: DD_LEADER_ELECTION
            value: "true"
#          - name: DD_LOGS_ENABLED
#            value: "true"
#          - name: DD_LOGS_CONFIG_CONTAINER_COLLECT_ALL
#            value: "true"
          - name : DZ_KONG_PORT  
            value: tcp://dz-kong:80
          - name : DZ_KONG_PORT_81_TCP_ADDR  
            value: dz-kong
          - name : DZ_KONG_PORT_80_TCP  
            value: tcp://dz-kong:80
          - name : DZ_KONG_PORT_443_TCP_ADDR  
            value: dz-kong
          - name : DZ_KONG_PORT_8444_TCP_ADDR  
            value: dz-kong
          - name : DZ_KONG_PORT_443_TCP  
            value: tcp://dz-kong:443
          - name : DZ_KONG_PORT_80_TCP_ADDR  
            value: dz-kong
          - name : DZ_KONG_PORT_81_TCP  
            value: tcp://dz-kong:81
          - name : DZ_KONG_SERVICE_HOST  
            value: dz-kong
          - name : DZ_KONG_PORT_8444_TCP  
            value: tcp://dz-kong:8444
          - name: KUBERNETES
            value: "yes"
          - name: DD_KUBERNETES_KUBELET_HOST
            valueFrom:
              fieldRef:
                fieldPath: status.hostIP
        resources:
          requests:
            memory: "128Mi"
            cpu: "100m"
          limits:
            memory: "512Mi"
            cpu: "250m"
        volumeMounts:
          - name: dockersocket
            mountPath: /var/run/docker.sock
          - name: procdir
            mountPath: /host/proc
            readOnly: true
          - name: cgroups
            mountPath: /host/sys/fs/cgroup
            readOnly: true
          - name: pointerdir
            mountPath: /opt/datadog-agent/run
#         - name: configs
#            mountPath: /conf.d
#            readOnly: true
          - name: passwd
            mountPath: /etc/passwd
            readOnly: true
          - name: dd-agent-config
            mountPath: /conf.d
        livenessProbe:
          exec:
            command:
            - ./probe.sh
          initialDelaySeconds: 15
          periodSeconds: 5
      volumes:
        - hostPath:
            path: /var/run/docker.sock
          name: dockersocket
        - hostPath:
            path: /proc
          name: procdir
        - hostPath:
            path: /sys/fs/cgroup
          name: cgroups
        - hostPath:
            path: /opt/datadog-agent/run
          name: pointerdir
#        - hostPath:
#            path: /opt/datadog-agent/conf.d
#          name: configs
        - hostPath:
            path: /etc/passwd
          name: passwd
        - name: dd-agent-config
          configMap:
            name: dd-agent-config
            items:
            - key: dz-redis-config
              path: redisdb.d/redisdb.yaml
            - key: dz-postgres-config
              path: postgres.d/postgres.yaml
            - key: dz-zookeeper-config
              path: zk.d/conf.yaml
#            - key: dz-mongo-config
#              path: mongo.d/conf.yaml
#            - key: dz-process-config
#              path: process.d/conf.yaml
#            - key: dz-kong-config
#              path: kong.d/conf.yaml

---

kind: ConfigMap
apiVersion: v1
metadata:
  name: dd-agent-config
  namespace: default
data:
  dz-redis-config: |-
    init_config:
    instances:
      - host: dz-redis
        port: 6379
  dz-postgres-config: |-
    init_config:
    instances:
      - host: dz-postgres
        port: 5432
        username: datadog
        password: 5qeI938pOPAg84m77ZmA9iHm
  dz-zookeeper-config: |-
    init_config:
    instances:
      - host: dz-zookeeper
        port: 2181
        timeout: 3
#  dz-mongo-config: |-
#    init_config:
#    instances:
#      - server: mongodb://datadog:mb8Z4f7mMlOzio86ro2BHZpe@dz-mongo:27016
#  dz-process-config: |-
#    init_config:
#    instances:
#      - name: connector-google-bigquery
#        search_string: ['java -jar build/libs/connector-google-bigquery-0.0.1-SNAPSHOT.jar']
#      - name: connector-google-analytics
#        search_string: ['java -jar build/libs/connector-google-analytics-0.0.1-SNAPSHOT.jar']
#      - name: connector-admin
#        search_string: ['java -jar build/libs/connector-admin-0.0.1-SNAPSHOT.jar']
#  dz-kong-config: |-
#    init_config:
#    instances:
#      - kong_status_url: http://dz-kong:8001/status/
