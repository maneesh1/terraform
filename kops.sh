export GOOGLE_APPLICATION_CREDENTIALS="/tmp/service-account.json" 
/usr/bin/gcloud auth activate-service-account --key-file /tmp/service-account.json
PROJECT=`/usr/bin/gcloud config get-value project`
export KOPS_FEATURE_FLAGS=AlphaAllowGCE
ssh-keygen -f $HOME/.ssh/id_rsa -t rsa -N ''
sleep 10
kops create cluster --cloud "gce"  --name maneeshtestdatazoom.k8s.local  --zones=us-central1-a  --master-zones=us-central1-a --node-size  n1-standard-8 --master-size n1-highmem-4   --state gs://kubernetes-cluster2 --project=datazoomjenkins  --node-count 1 --image dz-ubuntu-16-04-base-prod-1520946021
sleep 50s
echo " ########  Cluster creation in progress .... "
/usr/local/bin/kops update cluster maneeshtestdatazoom.k8s.local  --state  gs://kubernetes-cluster2/  --yes
echo "               - Lets wait for few minutes "
sleep 3m
STATUS='random'
STATE="Your cluster maneeshtestdatazoom.k8s.local is ready"
while [ "$STATUS" != "$STATE" ]; do
       echo "Lets wait for few min"
       sleep 1m
       kops validate cluster maneeshtestdatazoom.k8s.local --state gs://kubernetes-cluster2/ 2>&1 | grep maneeshtestdatazoom.k8s.local | grep ready > /tmp/stat
       STATUS=$( cat /tmp/stat | grep maneeshtestdatazoom.k8s.local | grep ready )
       echo "CHANGE Status :  " $STATUS 
done
echo "               - The wait is over You Can deploy the pods "
/usr/local/bin/kops validate cluster  maneeshtestdatazoom.k8s.local  --state   gs://kubernetes-cluster2/
