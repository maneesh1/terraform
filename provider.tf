provider "google" {
  credentials = "${file("/root/service-account.json")}"
  project     = "datazoomjenkins"
  region      = "${var.region}"
}
