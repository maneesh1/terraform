resource "google_compute_instance" "default" {
  name         = "maneeshtest"
  machine_type = "n1-highmem-4"
  zone         = "us-central1-a"
  tags = ["foo", "bar"]
  boot_disk {
    initialize_params {
      image = "dz-ubuntu-16-04-base-1520715380"
    }
 }
  metadata {
    ssh-keys = "ubuntu:${file("${var.public_key_path}")}"
  }
  provisioner "file" {
    source = "production"
    destination = "/tmp/production"
    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("${var.private_key_path}")}"
      agent = false
    }
}
  provisioner "file" {
    source = "kops.sh"
    destination = "/tmp/kops.sh"
    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("${var.private_key_path}")}"
      agent = false
    }
}

  provisioner "file" {
    source = "service-account.json"
    destination = "/tmp/service-account.json"
    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("${var.private_key_path}")}"
      agent = false
    }
}
provisioner "remote-exec" {
    connection {
      type = "ssh"
      user = "ubuntu"
      private_key = "${file("${var.private_key_path}")}"
      agent = false
    }
    inline = [
      "chmod +x /tmp/kops.sh",
      "/tmp/kops.sh"
    ]
  }
  // Local SSD disk
  scratch_disk {
  }
  network_interface {
    network = "default"
    access_config {
      // Ephemeral IP
    }
  }
}
output "ip" {
    value = "${google_compute_instance.default.network_interface.0.access_config.0.nat_ip}"
}
