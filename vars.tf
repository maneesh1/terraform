variable "region" {
  default = "us-central1-a"
}
variable "private_key_path" {
  default = "mykey"
}
variable "public_key_path" {
  default = "mykey.pub"
}
variable "platform-name" {
  default = "datazoom"
}
variable "gcloud-region" {
  default = "us-central1-a"
}
variable "test" {}
